<?php


namespace RSoftsTeam\Laravel\Permissions\Models\Traits;


use Illuminate\Routing\Route;

trait Permissions
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('RSoftsTeam\Laravel\Permissions\Models\Permission','users_permissions');
    }

    /**
     * @param string $permission
     * @param bool|null $deny
     * @return bool
     */
    public function hasPermission(string $permission, ?bool $deny = false): bool
    {
        $allow = !empty($this->permissions()->where('name', $permission)->first());
        return $deny ? !$allow : $allow;
    }

    /**
     * @param Route $route
     * @return bool
     */
    public function checkPermission(Route $route): bool
    {
        $data = [
            'class' => get_class($route->controller),
            'method' => $route->getActionMethod(),
            'http' => $route->methods[0],
        ];
        $data['method'] = $data['method'] === $data['class'] ? 'invoke' : $data['method'];

        return !empty($this->permissions()
            ->where('class', $data['class'])
            ->where('method',$data['method'])
            ->where('http','LIKE', "%".$data['http']."%")
            ->first());
    }
}
