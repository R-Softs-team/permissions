<?php

namespace RSoftsTeam\Laravel\Permissions;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use RSoftsTeam\Laravel\Permissions\Middleware\Permission as PermissionMiddleware;

class PermissionsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(Router $router)
    {
        $this->mergeConfigFrom(__DIR__.'./../config/permissions.php', 'permissions');
        $this->loadMigrationsFrom([
            __DIR__.'/../migrations/2019_07_10_000000_create_permissions_table.php',
            __DIR__.'/../migrations/2019_07_10_000000_create_users_permissions_table.php'
        ]);

        $router->aliasMiddleware('permission', PermissionMiddleware::class);

        Blade::if('permission', function (string $permission, ?bool $deny = false) {
            return Auth::user()->hasPermission($permission, $deny);
        });
    }

    public function register()
    {
        //
    }
}
