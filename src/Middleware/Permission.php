<?php


namespace RSoftsTeam\Laravel\Permissions\Middleware;


use Closure;
use Illuminate\Routing\Route;
use RSoftsTeam\Laravel\Permissions\Models\Permission as PermissionModel;

class Permission
{

    public function handle($request, Closure $next)
    {
        if (config('permission.auto_register', false))
            $this->registerPermissions($request->route());

        $user = $request->user();

        if (empty($user) || !$user->checkPermission($request->route()))
            abort(403);

        return $next($request);
    }

    public function registerPermissions(Route $route)
    {
        $data = [
            'name' => $route->getName(),
            'class' => get_class($route->controller),
            'method' => $route->getActionMethod(),
            'http' => $route->methods[0],
        ];

        $data['method'] = $data['method'] === $data['class'] ? 'invoke' : $data['method'];

        if (empty($permission = PermissionModel::where('class', $data['class'])->where('method', $data['method'])->first())) {
            $permission = new PermissionModel($data);
            $permission->save();
            $permission->users()->attach(1);

        } else {
            $methods = explode(',', $permission->http);

            if (!in_array($data['http'], $methods)) {
                $methods[] = $data['http'];
                $permission->http = implode(',', $methods);
                $permission->save();
            }
        }
    }
}
