# Использование

`$ composer require rsoftsteam/laravel-permissions`

`$ php artisan migrate`

##### Добавьте модели пользователя расширение
~~~~
...
use RSoftsTeam\Laravel\Permissions\Models\Traits\Permissions;

class User extends Authenticatable
{
    use Permissions, ...;
    ...
}
~~~~

##### Используйте вместе:
###### C маршрутами:
`Route::middleware([ 'permission' ]) ...`

###### C представлениями .blade.php:
~~~~
@permission('<route.name>')
    <!-- Выполнится при наличии прав у текущего пользователя --/>
@endpermission
~~~~
###### или
~~~~
@permission('<route.name>', true)
    <!-- Выполнится при отсутствии прав у текущего пользователя --/>
@endpermission
~~~~
